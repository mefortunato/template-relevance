#!/bin/bash
docker run -it --rm -p 8888:8888 \
	-v "$PWD/../:/work" \
	-w /work \
	-u $(id -u $USER):$(id -g $USER) \
	registry.gitlab.com/mefortunato/template-relevance bash -c "JUPYTER_CONFIG_DIR=/work/.jupyter JUPYTER_DATA_DIR=/work/.jupyter JUPYTER_RUNTIME_DIR=/work/.jupyter jupyter notebook --ip 0.0.0.0 --port 8888"
